package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
)

type UserBrowser struct {
	Browsers []string `json:"browsers"`
	Company  string
	Country  string
	Email    string `json:"email"`
	Job      string
	Name     string `json:"name"`
	Phone    string
}

// вам надо написать более быструю оптимальную этой функции
func FastSearch(out io.Writer) {
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}

	defer file.Close()

	seenBrowsers := make(map[string]bool)
	foundLine := -1
	u := &UserBrowser{}

	fmt.Fprintln(out, "found users:")

	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		foundLine++
		line := fileScanner.Bytes()

		err := json.Unmarshal(line, &u)
		if err != nil {
			panic(err)
		}

		isAndroid := false
		isMSIE := false

		for _, browser := range u.Browsers {
			if strings.Contains(browser, "Android") {
				isAndroid = true
				seenBrowsers[browser] = true
			}
			if strings.Contains(browser, "MSIE") {
				isMSIE = true
				seenBrowsers[browser] = true
			}
		}

		if !(isAndroid && isMSIE) {
			continue
		}

		email := strings.Replace(u.Email, "@", " [at] ", 1)
		fmt.Fprintf(out, "[%d] %s <%s>\n", foundLine, u.Name, email)

	}
	fmt.Fprintln(out)
	fmt.Fprintln(out, "Total unique browsers", len(seenBrowsers))
}
